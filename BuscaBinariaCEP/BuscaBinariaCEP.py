import struct
import sys
import os

#Aluna : Thaina Simoes Pires
#Busca Binaria
#Python 2.7.12


if len(sys.argv) != 2:
	print "USO %s [CEP]" % sys.argv[0]
	quit()

registroCEP = struct.Struct("72s72s72s72s2s8s2s")
cepColumn = 5
print "Tamanho da Estrutura: %d" % registroCEP.size
f = open("cep_ordenado.dat","r")


inicio = 0
fim = os.path.getsize("cep_ordenado.dat")/registroCEP.size
meio = (inicio + fim) / 2
count = 0


while inicio <= fim:
	count = count + 1
	f.seek(meio * registroCEP.size, 0)
	line = f.read(registroCEP.size)
	line_t = registroCEP.unpack(line)
	if line_t[cepColumn] == sys.argv[1]:
		for i in range(0,len(line_t)-1):
			print line_t[i] # .decode('latin1')
		print "Quantidade de acessos %i" % count
		break
	elif line_t[cepColumn] > sys.argv[1]:
		fim = meio -1
		meio = (inicio + fim) / 2
	elif line_t[cepColumn] < sys.argv[1]:
		inicio = meio + 1
		meio = (inicio + fim) / 2

print "Fim do programa"
f.close()
